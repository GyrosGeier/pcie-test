#! /bin/sh -x

set -e

ghdl -a --std=08 sha256.vhdl tb_sha256.vhdl
ghdl -e --std=08 tb_sha256
ghdl -r --std=08 tb_sha256 --vcd=tb_sha256.vcd
vcd2fst tb_sha256.vcd tb_sha256.fst

ghdl -a --std=08 pcie_arbiter.vhdl dma_read.vhdl dma_write.vhdl sha_fifo_sim.vhdl sha256.vhdl command_processor.vhdl tb_command_processor.vhdl
ghdl -e --std=08 tb_command_processor
ghdl -r --std=08 tb_command_processor --vcd=tb_command_processor.vcd
vcd2fst tb_command_processor.vcd tb_command_processor.fst

library ieee;

use ieee.std_logic_1164.ALL;

entity dma_read is
	generic(
		tag : std_logic_vector(7 downto 0)
	);
	port(
		clk : in std_logic;
		reset_n : in std_logic;
		
		cfg_address : std_logic_vector(15 downto 0);

		rx_ready : out std_logic;
		rx_valid : in std_logic;
		rx_data : in std_logic_vector(63 downto 0);
		rx_sop : in std_logic;
		rx_eop : in std_logic;
		rx_err : in std_logic;
		rx_mask : out std_logic;
		-- valid during second cycle in 64 bit interface
		rx_bardec : in std_logic_vector(7 downto 0);

		
		tx_req : out std_logic;
		tx_start : in std_logic;
		
		tx_ready : in std_logic;
		tx_valid : out std_logic;
		tx_data : out std_logic_vector(63 downto 0);
		tx_sop : out std_logic;
		tx_eop : out std_logic;
		tx_err : out std_logic;
		
		cpl_pending : out std_logic;
		
		address : in std_logic_vector(63 downto 0);
		size : in std_logic_vector(63 downto 0);
		strobe : in std_logic;

		data_valid : out std_logic;
		data_data : out std_logic_vector(63 downto 0);

		dma_done : out std_logic
	);
end entity;

architecture syn of dma_read is
	type state is (idle, waiting, header, stop);
	
	signal tx_state : state;

	signal set_dma_done : std_logic;
	
	constant nullptr : std_logic_vector(63 downto 0) := (others => '0');
begin
	rx_ready <= '1';
	rx_mask <= '0';
	cpl_pending <= '0';
	tx_err <= '0';
	
	tx_req <= '1' when (tx_state = waiting)
			else '0';

	process(reset_n, clk)
	begin
		if(reset_n = '0') then
			tx_state <= idle;
			tx_valid <= '0';
			tx_sop <= 'U';
			tx_eop <= 'U';
			tx_data <= (others => 'U');
		elsif(rising_edge(clk)) then
			if(tx_ready = '1') then
				case tx_state is
					when idle =>
						if(strobe = '1') then
							tx_state <= waiting;
						end if;
					when waiting =>
						if(tx_start = '1') then
							tx_valid <= '1';
							tx_sop <= '1';
							tx_eop <= '0';
							tx_data <=
									cfg_address &			-- requester id
									tag &			-- tag
									"11111111" &			-- byte enable
									"00100000" &			-- fmt/type
									"0" &						-- reserved
									"000" &					-- tc
									"0000" &					-- reserved
									"0" &						-- no digest
									"0" &						-- not poisoned
									"00" &					-- attributes
									"00" &					-- reserved
									size(11 downto 2);			-- length
									-- TODO: more than 4kB
							tx_state <= header;
						else
							tx_valid <= '0';
							tx_sop <= 'U';
							tx_eop <= 'U';
							tx_data <= (others => 'U');							
						end if;
					when header =>
						tx_valid <= '1';
						tx_sop <= '0';
						tx_eop <= '1';
						tx_data <=
								address(31 downto 2) &
								"00" &
								address(63 downto 32);
						tx_state <= stop;
					when stop =>
						tx_valid <= '0';
						tx_sop <= 'U';
						tx_eop <= 'U';
						tx_data <= (others => 'U');
						if(set_dma_done = '1') then
							tx_state <= idle;
						end if;
				end case;
			else
				tx_valid <= '0';
				tx_sop <= 'U';
				tx_eop <= 'U';
				tx_data <= (others => 'U');
			end if;
		end if;
	end process;

	-- handle completions
	process(reset_n, clk) is
		type state_t is (header1, header2, data, idle);

		variable state : state_t;
	begin
		if(reset_n = '0') then
			set_dma_done <= '1';
			data_valid <= '0';
			state := idle;
		elsif(rising_edge(clk)) then
			set_dma_done <= '0';
			if(rx_valid = '0') then
				data_valid <= '0';
				data_data <= (others => 'U');
			else
				if(rx_sop = '1') then
					state := header1;
				end if;
				case state is
					when header1 =>
						data_valid <= '0';
						if(rx_data(31 downto 24) = "01001010") then		-- completion with data
							-- we are the only component so far
							state := header2;
						else
							state := idle;
						end if;
					when header2 =>
						data_valid <= '0';
						if(rx_eop = '1') then
							state := idle;		-- should not happen
						elsif(rx_data(15 downto 8) /= tag) then
							state := idle;
						else
							state := data;
						end if;
					when data =>
						data_valid <= '1';
						data_data <= rx_data;
						if(rx_eop = '1') then
							set_dma_done <= '1';
							state := idle;
						end if;
					when idle =>
						data_valid <= '0';
				end case;
			end if;
		end if;
	end process;

	dma_done <= '0' when reset_n = '0' else
		    '0' when strobe = '1' and rising_edge(clk) else
		    '1' when set_dma_done = '1' and rising_edge(clk) else
		    unaffected;
end architecture;

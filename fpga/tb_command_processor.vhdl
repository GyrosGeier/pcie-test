library ieee;
use ieee.std_logic_1164.ALL;

library std;
use std.env.finish;

entity tb_command_processor is
end entity;

architecture sim of tb_command_processor is
	signal clk : std_logic := '0';
	signal reset_n : std_logic;

	subtype qword is std_logic_vector(63 downto 0);

	signal ready : std_logic;
	signal valid : std_logic;
	signal data : qword;

	signal pcie_rx_ready : std_logic;
	signal pcie_rx_valid : std_logic;
	signal pcie_rx_data : qword;
	signal pcie_rx_sop : std_logic;
	signal pcie_rx_eop : std_logic;
	signal pcie_rx_err : std_logic;

	signal pcie_rx_mask : std_logic;
	signal pcie_rx_bardec : std_logic_vector(7 downto 0);

	signal pcie_tx_req : std_logic;
	signal pcie_tx_start : std_logic;

	signal pcie_tx_ready : std_logic;
	signal pcie_tx_valid : std_logic;
	signal pcie_tx_data : qword;
	signal pcie_tx_sop : std_logic;
	signal pcie_tx_eop : std_logic;
	signal pcie_tx_err : std_logic;

	signal pcie_cpl_pending : std_logic;

	signal int : std_logic;

	constant cfg_address : std_logic_vector(15 downto 0) := x"0100";	-- bus 1, device 0, function 0
begin
	-- simulation timeout
	process
	begin
		wait for 2 us;
		report "simulation timeout" severity error;
		finish;
	end process;

	-- clock (100 MHz)
	clk <= not clk after 5 ns;

	-- stimuli
	process
	begin
		reset_n <= '0';
		wait for 30 ns;
		pcie_tx_ready <= '1';
		pcie_rx_valid <= '0';
		valid <= '0';
		wait for 20 ns;
		reset_n <= '1';

		-- command 1: SHA256 Init
		wait until falling_edge(clk) and ready = '1';
		data <= x"01000000_00000001";
		valid <= '1';
		wait until falling_edge(clk);
		data <= x"01000000_00000002";
		wait until falling_edge(clk);
		data <= x"12345678_00000000";
		wait until falling_edge(clk);
		data <= x"00000000_00000010";
		wait until falling_edge(clk);
		data <= (others => 'U');
		valid <= '0';

		wait until rising_edge(clk) and pcie_tx_valid = '1';
		assert pcie_tx_data = x"010001ff_20000004" report "first packet not expected read request" severity error;
		assert pcie_tx_sop = '1' report "first data word not start of packet" severity error;
		assert pcie_tx_eop = '0' report "first data word is end of packet" severity error;
		assert pcie_tx_err = '0' report "first data word has error marker" severity error;
		wait until rising_edge(clk) and pcie_tx_valid = '1';
		assert pcie_tx_data = x"00000000_12345678" report "first packet not expected address" severity error;
		assert pcie_tx_sop = '0' report "second data word is start of packet" severity error;
		assert pcie_tx_eop = '1' report "second data word is not end of packet" severity error;
		assert pcie_tx_err = '0' report "second data word has error marker" severity error;
		wait until rising_edge(clk);

		wait until falling_edge(clk) and pcie_rx_ready = '1';
		pcie_rx_valid <= '1';
		pcie_rx_data <= x"00000016_4a000004";
		pcie_rx_sop <= '1';
		pcie_rx_eop <= '0';
		pcie_rx_err <= '0';
		wait until falling_edge(clk) and pcie_rx_ready = '1';
		pcie_rx_data <= x"00000000_01000100";
		pcie_rx_sop <= '0';
		wait until falling_edge(clk) and pcie_rx_ready = '1';
		pcie_rx_data <= x"894c84feb8e83cfa";
		wait until falling_edge(clk) and pcie_rx_ready = '1';
		pcie_rx_data <= x"39fd73c3efe0dc03";
		pcie_rx_eop <= '1';
		wait until falling_edge(clk) and pcie_rx_ready = '1';
		pcie_rx_valid <= '0';
		pcie_rx_data <= (others => 'U');
		pcie_rx_sop <= 'U';
		pcie_rx_eop <= 'U';
		pcie_rx_err <= 'U';

		wait until falling_edge(clk) and ready = '1';
		data <= x"01000000_00000003";
		valid <= '1';
		wait until falling_edge(clk) and ready = '1';
		data <= x"12345678_00000000";
		wait until falling_edge(clk);
		valid <= '0';
		data <= (others => 'U');

		wait until rising_edge(clk) and pcie_tx_valid = '1';
		assert pcie_tx_data = x"010000ff_60000008" report "second packet not expected write request" severity error;
		assert pcie_tx_sop = '1' report "first data word not start of packet" severity error;
		assert pcie_tx_eop = '0' report "first data word is end of packet" severity error;
		assert pcie_tx_err = '0' report "first data word has error marker" severity error;
		wait until rising_edge(clk) and pcie_tx_valid = '1';
		assert pcie_tx_data = x"00000000_12345678" report "second packet not expected address" severity error;
		assert pcie_tx_sop = '0' report "second data word is start of packet" severity error;
		assert pcie_tx_eop = '0' report "second data word is end of packet" severity error;
		assert pcie_tx_err = '0' report "second data word has error marker" severity error;
		wait until rising_edge(clk) and pcie_tx_valid = '1';
		assert pcie_tx_data = x"8fa0cb8aeeea79f2" report "second packet not expected data" severity error;
		assert pcie_tx_sop = '0' report "intermediate data word is start of packet" severity error;
		assert pcie_tx_eop = '0' report "intermediate data word is end of packet" severity error;
		assert pcie_tx_err = '0' report "intermediate data word has error marker" severity error;
		wait until rising_edge(clk) and pcie_tx_valid = '1';
		assert pcie_tx_data = x"89d48b1a8978aba0" report "second packet not expected data" severity error;
		assert pcie_tx_sop = '0' report "intermediate data word is start of packet" severity error;
		assert pcie_tx_eop = '0' report "intermediate data word is end of packet" severity error;
		assert pcie_tx_err = '0' report "intermediate data word has error marker" severity error;
		wait until rising_edge(clk) and pcie_tx_valid = '1';
		assert pcie_tx_data = x"13f604009985b403" report "second packet not expected data" severity error;
		assert pcie_tx_sop = '0' report "intermediate data word is start of packet" severity error;
		assert pcie_tx_eop = '0' report "intermediate data word is end of packet" severity error;
		assert pcie_tx_err = '0' report "intermediate data word has error marker" severity error;
		wait until rising_edge(clk) and pcie_tx_valid = '1';
		assert pcie_tx_data = x"4051288b661a5620" report "second packet not expected data" severity error;
		assert pcie_tx_sop = '0' report "last data word is start of packet" severity error;
		assert pcie_tx_eop = '1' report "last data word is not end of packet" severity error;
		assert pcie_tx_err = '0' report "last data word has error marker" severity error;
		wait until rising_edge(clk);

		wait until falling_edge(clk) and ready = '1';
		data <= x"00000000_00000001";
		valid <= '1';
		wait until falling_edge(clk);
		valid <= '0';
		data <= (others => 'U');

		if(int /= '1') then
			wait until int = '1';
		end if;

		wait for 100 ns;

		finish;

		wait;
	end process;

	-- immediately grant bus access
	-- TODO: pull into test
	pcie_tx_start <= pcie_tx_req;

	-- dut
	dut : entity work.command_processor
		port map(
			clk => clk,
			reset_n => reset_n,

			ready => ready,
			valid => valid,
			data => data,

			pcie_rx_ready => pcie_rx_ready,
			pcie_rx_valid => pcie_rx_valid,
			pcie_rx_data => pcie_rx_data,
			pcie_rx_sop => pcie_rx_sop,
			pcie_rx_eop => pcie_rx_eop,
			pcie_rx_err => pcie_rx_err,

			pcie_rx_mask => pcie_rx_mask,
			pcie_rx_bardec => pcie_rx_bardec,

			pcie_tx_req => pcie_tx_req,
			pcie_tx_start => pcie_tx_start,

			pcie_tx_ready => pcie_tx_ready,
			pcie_tx_valid => pcie_tx_valid,
			pcie_tx_data => pcie_tx_data,
			pcie_tx_sop => pcie_tx_sop,
			pcie_tx_eop => pcie_tx_eop,
			pcie_tx_err => pcie_tx_err,

			pcie_cpl_pending => pcie_cpl_pending,

			cfg_address => cfg_address,

			int => int
		);
end architecture;

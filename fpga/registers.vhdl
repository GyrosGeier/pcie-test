library ieee;

use ieee.std_logic_1164.ALL;

entity registers is
	port(
		reset : in std_logic;
		clk : in std_logic;

		cfg_address : in std_logic_vector(15 downto 0);

		rx_ready : out std_logic;
		rx_valid : in std_logic;
		rx_data : in std_logic_vector(63 downto 0);
		rx_sop : in std_logic;
		rx_eop : in std_logic;
		rx_err : in std_logic;
		rx_mask : out std_logic;
		-- valid during second cycle in 64 bit interface
		rx_bardec : in std_logic_vector(7 downto 0);

		tx_ready : in std_logic;
		tx_valid : out std_logic;
		tx_data : out std_logic_vector(63 downto 0);
		tx_sop : out std_logic;
		tx_eop : out std_logic;
		tx_err : out std_logic;
		
		tx_req : out std_logic;
		tx_start : in std_logic;

		cpl_pending : out std_logic;

		reg_cmd_queue_base : out std_logic_vector(63 downto 0);
		reg_cmd_queue_valid : out std_logic_vector(63 downto 0)
	);
end entity;

architecture syn of registers is
	type rx_state_type is (idle, header, data);
	signal rx_state : rx_state_type;

	type tx_state_type is (idle, header, data, finish);
	signal tx_state : tx_state_type;

	type pkt_type is (MRd, MWr);

	subtype completion_status is std_logic_vector(2 downto 0);
	constant SC : completion_status := "000";
	constant UR : completion_status := "001";
	constant CRS : completion_status := "010";
	constant CA : completion_status := "100";

	subtype qword is std_logic_vector(63 downto 0);

	subtype tc is std_logic_vector(2 downto 0);
	subtype attr is std_logic_vector(2 downto 0);
	subtype requester_id is std_logic_vector(15 downto 0);
	subtype tag is std_logic_vector(7 downto 0);

	-- saved values (first to second cycle)
	signal hdr_pkt_type : pkt_type;
	signal hdr_tc : tc;
	signal hdr_attr : attr;
	signal hdr_requester_id : requester_id;
	signal hdr_tag : tag;
	signal hdr_length : std_logic_vector(9 downto 0);

	-- queued completion (single QWORD completer)
	-- TODO: more than one
	-- TODO: this is the "FIFO not empty" signal
	signal tx_req_int : std_logic;

	signal cpl_tc : tc;
	signal cpl_attr : attr;
	signal cpl_requester_id : requester_id;
	signal cpl_tag : tag;
	signal cpl_status : completion_status;
	signal cpl_laddr : std_logic_vector(6 downto 0);
	signal cpl_data : qword;

	type registers is (cmd_queue_base, cmd_queue_valid);

	signal selected_register : registers;

	-- register addresses
	constant addr_cmd_queue_base : std_logic_vector(7 downto 0) := "00000000";
	constant addr_cmd_queue_valid : std_logic_vector(7 downto 0) := "00001000";
	-- register values
	signal reg_cmd_queue_base_int : std_logic_vector(63 downto 0);
	signal reg_cmd_queue_valid_int : std_logic_vector(63 downto 0);

begin
	rx_ready <= '1';

	-- assumption: we are never busy
	rx_mask <= '0';
	
	tx_req <= tx_req_int;

	-- tx_req is deasserted at sop of last packet, extend until eop
	-- for "completion pending" signal
	cpl_pending <= '1' when (tx_state /= idle) else tx_req_int;

	-- TODO: error handling
	tx_err <= '0';

	process(reset, clk)
		-- set has priority over reset
		variable tx_req_set : boolean;
		variable tx_req_reset : boolean;
	begin
		if(reset = '0') then
			rx_state <= idle;
			tx_state <= idle;
			tx_req_int <= '0';
			reg_cmd_queue_base_int <= (others => '0');
			reg_cmd_queue_valid_int <= (others => '0');
		elsif(rising_edge(clk)) then
			tx_req_set := false;
			tx_req_reset := false;
			if(rx_valid = '1') then
				-- start of packet resets state machine
				if(rx_sop = '1') then
					rx_state <= header;
					case rx_data(31 downto 24) is
						when "00000000"|"00100000" =>
							hdr_pkt_type <= MRd;
						when "01000000"|"01100000" =>
							hdr_pkt_type <= MWr;
						when others =>
							rx_state <= idle;
					end case;
					hdr_tc <= rx_data(22 downto 20);
					hdr_attr <= rx_data(18) & rx_data(13 downto 12);
					hdr_length <= rx_data(9 downto 0);
					hdr_requester_id <= rx_data(63 downto 48);
					hdr_tag <= rx_data(47 downto 40);
				else
					case rx_state is
						when idle =>
							-- should never get here
							null;
						when header =>
							case hdr_pkt_type is
								when MRd =>
									-- prepare completion
									cpl_tc <= hdr_tc;
									cpl_attr <= hdr_attr;
									cpl_requester_id <= hdr_requester_id;
									cpl_tag <= hdr_tag;
									cpl_laddr <= rx_data(6 downto 0);
									-- no more data
									rx_state <= idle;
								when MWr =>
									rx_state <= data;
							end case;

							-- TLP cycle 2
							-- For both supported packet types, this is the address cycle.

							-- Altera PCIe HIP pads 32 bit addresses to 64 bits,
							-- so no distinction for 3DW/4DW packets needs to be made
							-- BAR decoding is done in HIP block, we decode that output
							-- together with lower bits
							if(rx_bardec(0) = '1') then
								case rx_data(7 downto 0) is
									when addr_cmd_queue_base =>
										case hdr_pkt_type is
											when MRd =>
												tx_req_set := true;
												cpl_status <= SC;
												cpl_data <= reg_cmd_queue_base_int;
											when MWr =>
												selected_register <= cmd_queue_base;
										end case;
									when addr_cmd_queue_valid =>
										case hdr_pkt_type is
											when MRd =>
												tx_req_set := true;
												cpl_status <= SC;
												cpl_data <= reg_cmd_queue_valid_int;
											when MWr =>
												selected_register <= cmd_queue_valid;
										end case;
									when others =>
										case hdr_pkt_type is
											when MRd =>
												tx_req_set := true;
												cpl_status <= UR;
												cpl_data <= (others => 'U');
											when MWr =>
												-- ignore remainder of packet
												rx_state <= idle;
										end case;
								end case;
							end if;
						when data =>
							if(rx_eop = '1') then
								rx_state <= idle;
							end if;
							case selected_register is
								when cmd_queue_base =>
									reg_cmd_queue_base_int <= rx_data;
								when cmd_queue_valid =>
									reg_cmd_queue_valid_int <= rx_data;
							end case;
					end case;
				end if;
			end if;

			-- tx path
			-- TODO: output side of FIFO
			if(tx_ready = '1') then
				case tx_state is
					when idle|finish =>
						if(tx_start = '1') then
							tx_sop <= '1';
							tx_eop <= '0';
							tx_valid <= '1';
							tx_data <=
									cfg_address &				-- completer id
									cpl_status &				-- completion status
									"0" &							-- BCM
									"000000001000" &			-- byte count TODO
									"01001010"	&				-- fmt/type
									"0" &							-- reserved
									cpl_tc &						-- TC
									"0" &							-- reserved
									cpl_attr(2)	&				-- attributes[2:2]
									"0" &							-- reserved
									"0" &							-- TH
									"0" &							-- TD
									"0" &							-- EP
									cpl_attr(1 downto 0) &	-- attributes[1:0]
									"00" &						-- AT
									"0000000010";				-- length
							tx_req_reset := true;
							tx_state <= header;
						else
							tx_sop <= 'U';
							tx_eop <= 'U';
							tx_valid <= '0';
							tx_data <= (others => 'U');
							tx_state <= idle;
						end if;
					when header =>
						tx_sop <= '0';
						tx_eop <= '0';
						tx_valid <= '1';
						-- Altera PCIe HIP ignores these and expects data on the
						-- next cycle
						tx_data(63 downto 32) <= (others => '0');
						tx_data(31 downto 0) <=
								cpl_requester_id &		-- requester id
								cpl_tag &					-- tag
								"0" &							-- reserved
								cpl_laddr;					-- lower address bits
						tx_state <= data;
					when data =>
						tx_sop <= '0';
						tx_eop <= '1';
						tx_valid <= '1';
						tx_data <= cpl_data;
						tx_state <= finish;
				end case;
			else
				tx_sop <= 'U';
				tx_eop <= 'U';
				tx_valid <= '0';
				tx_data <= (others => 'U');
			end if;

			if(tx_req_set) then
				tx_req_int <= '1';
			elsif(tx_req_reset) then
				tx_req_int <= '0';
			end if;
		end if;
	end process;

	reg_cmd_queue_base <= reg_cmd_queue_base_int;
	reg_cmd_queue_valid <= reg_cmd_queue_valid_int;
end architecture;

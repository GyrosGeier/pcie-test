library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

library std;
use std.env.finish;

entity tb_sha256 is
end entity;

architecture sim of tb_sha256 is
	signal reset_n : std_logic;
	signal ck : std_logic := '0';

	signal ready, init, update, final : std_logic;
	signal data : std_logic_vector(31 downto 0);
	signal be : std_logic_vector(3 downto 0);

	signal out_valid : std_logic;
	signal out_data : std_logic_vector(31 downto 0);
begin
	-- sim timeout
	process
	begin
		wait for 1 ms;
		report "Simulation timeout" severity error;
		finish;
	end process;

	-- clock
	ck <= not ck after 5 ns;

	-- dut
	checksum : entity work.sha256
		port map(
			reset_n => reset_n,
			ck => ck,

			ready => ready,
			init => init,
			update => update,
			final => final,
			data => data,
			be => be,

			out_data => out_data,
			out_valid => out_valid
		);

	-- stimuli
	process
		procedure idle is
		begin
			init <= '0';
			update <= '0';
			final <= '0';
			data <= (others => 'U');
			be <= (others => 'U');
		end procedure;

		procedure tick is
		begin
			wait until rising_edge(ck) and ready = '1';
		end procedure;

		type randombytes_t is array(0 to 31) of std_logic_vector(31 downto 0);
		constant randombytes : randombytes_t :=
		(
			x"ff55826e", x"4f3f0627", x"010c0d84", x"b045b8a1",
			x"c916016c", x"7e24d9be", x"ea862323", x"801e7c79",
			x"601bfaf8", x"b26cb409", x"fe9b0ddd", x"b0659113",
			x"94d087b0", x"a062d425", x"7e5c3c2a", x"01679a99",
			x"1bd7db1e", x"4021b24d", x"9f4f35c2", x"370ee8c1",
			x"08240130", x"9cf4807f", x"5f0a6829", x"18138477",
			x"42208427", x"8ae2424b", x"4330cdba", x"850a90a0",
			x"42ac722b", x"e5c5effd", x"bd25a6fb", x"2291ad42"
		);

		type expected_t is array(0 to 128) of std_logic_vector(255 downto 0);
		constant expected : expected_t :=
		(
			x"e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
			x"a8100ae6aa1940d0b663bb31cd466142ebbdbd5187131b92d93818987832eb89",
			x"3b92072e48a61c9bb81520696591f08d8be7dd8b1889e933b633af651ddd7643",
			x"ae398757bf401318fc686a388a8c42690c9377964d578676798874406107087d",
			x"661e7db6d08d5a1ce30cc66b9d503b40798c641da880a5e05a9477b421b3e38b",
			x"ec5b900db40a76a4ee38d1935e0d6e0530fbf39ec1e68743905652374ebd8a11",
			x"b2b95391eeff00ac0f92b1d670dc58effa340676c9487c5b8f54913d71c22c54",
			x"00b1c37c998d4f17378de19011dd9820446920759114e22dd90f35fe3f99929f",
			x"ead9f9f490f98d946b14737d4399590f7db9ab40b24901add984bb7595f4e05d",
			x"6fe0cbac02d55b15702b82fb62aeb262c6c11e367ff0c2ffff83b75ed0a2343c",
			x"97721ebefce07e34b2d5d06fccc045d257cf3bd1d49ed0cb10e48e2a18940ed8",
			x"7ab26d9c48eef9e9f753945d0d08859f53719301ea8653c81c90d705ae3ddd45",
			x"e140dea5865574702abc9b18aa43e856d9304aa7271f4e112fff626b4c32c03e",
			x"08bdd40c79e52c278180caf4eb8b6957c8de8cec31df847449d1d25ab0b95af1",
			x"06af51f212ff61297d6302f136cd362bf3c2d7040d6251ce1da59e6e729f5f8a",
			x"efa4891c5edc6c79093f9fda7eb7d9df0fa62233dfbb4925209dc240b8560c48",
			x"c7db5da8e5b72a07c9d1613e1c6c7726797999887b239b5f3f442e826e6b1f81",
			x"40d612fa41fb82c8616bd30d3b755e2cbc0a224cb6bc169e5213e571a573af95",
			x"e268657dedf120102e77d131b0f4756fb7ed6473a89481decfb895de67f85d81",
			x"2df7a9b34f4a86382ea080471cd0d875d81213bf9d6798ff695a3cabb5d627e8",
			x"ee5dce364d5337000fc165f74c7d460c475d77f1f418019fe672ca91d483c17a",
			x"c83dd7f03596f1d4991b75edb64ceb6b138336b0537e02ab173dcd8638be108b",
			x"faf4699950a8b7c8f064f55345956ba4238feb9458048d82a5adc721d15c206c",
			x"25bdf931349b11ccbae79905fc7c56b15e26668a7f001133470103bcefc8ab60",
			x"3e81f2bf871f799f2c5c115f1cd0944ddcdb2e7b73fcfc991c063f92cea995fa",
			x"7bc5054a735f6c5ab0364497447c50c14b039bc06f69527b201336371d84a5d0",
			x"ed0ecb06bda9bc5bebb9af8350494fb110a15815355bfa3aff7726b56f5c5dbe",
			x"792fef8aa8f934e88acd3e1bbbe824ade86b70d25e1b47de383359c22f2b23cd",
			x"6e975592e3b112aa6e31e7b2c2e480661eb61f514056a50e04e54745ecb0896f",
			x"2395d6705318db0f95b1a910fd05d666c30f54aac6d2765b0384f57ae62730ab",
			x"027b011e145c6e1a40a010ac9005e74ad5a7ef64d12e8b9ae46b714a462bedc8",
			x"423388de5470a544968d62a63bbb68a4e95a694f7a8311546023eeaaab51457e",
			x"58f6e10a94fb97b97261370ae3b4961728a9ad9158496d6004aa37e6f5e25381",
			x"c30867201ba301a62204bbcaa2360dd1287361f50e1d1c7cf83cc0116cbb987b",
			x"3db9e9311265b0585253485ed8976063e2c04f6f3e7d6fb1f8ce639d44d10232",
			x"e271ab5b81e63be60c774147061356eefbb4196b5fded16f295f9cbb5054e574",
			x"037d9fe11d86c38ee4fe4f0864e6550558199416730dd57fe210cc17d89e687a",
			x"fb263eaa2177ec5d182c7019e5429548494848505e4a644afd5811c17ad32979",
			x"17ed638c0401ebec07f743ce965ba56543c64a740deb7b3aa36efb68e28a0745",
			x"9ee7e0eba2e46cbbde976044bf928843e7d5dc24644ef98bc041a5ffb2b8d039",
			x"921c39bb3cd6b1af86c00d212b8f9ed386c4466d9f64e9cf4ed8ec54c53d238b",
			x"23e1b39827b5b3b754734b75c0c64fb45b9865be09b221950921c8333f007e74",
			x"98fc9464e533a63c9c3c4df4b04deb975cb2e543cc0548bb612c662c3160f6e5",
			x"2a3e57d407733f907958230c43f1b29a3157cd89cdb8e4360f16b8459e6cb693",
			x"68ce023548e9b31e4c52dde411e7270bb48bfdf309ffb20bb57552b3f5607c23",
			x"fdfba15def3ad5aec1d76e8c4e9ab751531e268a258c52e2efeed4649454a9ca",
			x"1a6b0094d4540a228cb2f1a69a111ae6208d87f8c639d85e861aaf39d9563b2b",
			x"69ddd82220a1330fa8d384a1e4d1e650d6339c6d256cf1eec6f0f57d2a47738f",
			x"51d3a3759c79c8bec072eabe9565f935b7e2d753b482d8d5b5bc4def7fa8a2d5",
			x"378ef3b63fc41ffcf26e6b4515a798b485bf8f5e2089a78095854990337d9f3e",
			x"b9460a123046759851d85e608c4020c1eecec8dc149c50b7cc8395ae63cd41c3",
			x"05dee10437c7e0c0db4c47c996b0b1870e2464404fbe7486836ac9d0b6135284",
			x"68aaa7e667f1c88824579d4d6d46df957e8095a9531a6191e105683bb048f857",
			x"89b074c0d7b839fff4e18979a79832aa1f9a5a205ea9f10d03a027cca8a25d07",
			x"63a85e2b874633e61c41f378f3747c40c1517520e802de1aeeaca047d8b19c5c",
			x"f520c0c4cd8164874ab07f93b0c20488b66c8b4d361ab3dc327169ff6d9f8d5d",
			x"537cbabb49ace5aff4963c2ccb2a7a6d70942aa999b323f6e8eb791b1ac173ee",
			x"50b1c303549a2f2cd4b21100126169d31a2d203daf88f07c33dd1fb0e4fcf98d",
			x"9b826a48fcabb76a66399ab76785f24468c83824b845ee6664fbe3cd22dd5300",
			x"f9d132e1e35e049fb479ea307d968af38a385a49c5b927a87575a1d8134e6334",
			x"45a1282fce0d457e62a3c9cbe5df524532b66bea2e841cef30d12323a54b16b0",
			x"32788fa92882be7d9b944529a4e9fb34f00831ac137b175b1767b0564cd26be1",
			x"80c85cd6d9b3f4890d09a2315c6ae01b75dd50ee84431d8953125f8c26cb8e80",
			x"34ca02d4bbec6384ae8bffeb08caf8df0a8fa503ad22cbc1346a15d8c1977951",
			x"3f81f4c61926091ff2b6c2aa12e7cc81033be9378e898a035fd70704a140eef9",
			x"6b04aebca9dbb7d279b8967094123b9cf01acbbcbd5296abac4f656ba5f3a8c3",
			x"80bfc5a6bd5330aad94fd5e11e11715ffc172eedb0f968b09ab8bd5879032407",
			x"db12c749ece7818f5d6a9af5b4fbe9f79c487e3a169bf67c074b0d23376f697c",
			x"97edf0011ece3fa9ad1873e7e701957b691d5d841f22aab1101cb07fc29ead98",
			x"77fcff2479ade5735f5aeb25eee5eac739864a2e133b23f7a7e2722d938adb30",
			x"a6205daa30cc2fce66cbf69264115778e128bf195a6645721d7922abd70a242f",
			x"da49dce599d6149de7f170c59574c391ab0256e31d98d7244f571d9ae2f0be78",
			x"42ec7e09b132bc2d575e1c9707419cd906f3e552344211f42766402998e6c1e4",
			x"ad84ef6a93a7cdbd376faa2669bc039a613ea9c57ab360c49a426c1b17388d51",
			x"8190b868d668b968fbc0032c3b0424ae610ef49f231d86355c10d0547fb44be8",
			x"2be57f554d14b8cf53a6c5de82c7e4b935022519a4c366cb8f83235a12b10985",
			x"5b96293b76c087befdfc41de7a57db29d575d9a29152da5817a290bff421fb83",
			x"bf5d8e571b00c7d6275b2a7f592430c42f0ca34e46b4debe72b61801c5d59e9d",
			x"d40e7fb20448be603d3e037d84912a2dd4c9511acc0082dc8803834966aa86d9",
			x"df527173c4fe1e1443ae88b69add778e22c9662964e787e2af0c37b51a7dd76c",
			x"81991335e80550231a82d1307de2d5b5a7c83025ee572761484f2fd6e151b15c",
			x"0d41a2b0388e2812d0997ef1be526747ea22df8ea51d60ce360a8ebd281903e9",
			x"f68ef73c6807a99957617e164014823b500775f98ceb43a42ce5fffb089c8de3",
			x"6d361497edf010b61ab448134fe7d45b0517f6c43f491e2a5ee1ea791731b6cf",
			x"1b407032bcc7fbc596dddb1d96d095eb3257f6f1e6311dc1270122f8f3c2ed37",
			x"ecee053a82ee0d09519fcbb5c808b34000adb5c06298057d463e8aa95dc20fe6",
			x"f1009ecb60d28a467999d712aef77ff397f8e426752f9590f4831612d1176957",
			x"9257bfa6552acab098d4285f9f5af2c0bb49967532cef64a0a12ffba13e37fa9",
			x"6891f6dca876051a81279a35a797f632e0de669fff52bc066572bebc3e1e0ecd",
			x"cdcfee7d03d106bb6536a179d515f8269a69a85782e2c6136d8dba868fbb5b9e",
			x"0e04ef33d90f17b124e2c626d007f3c33b3b0e207dfb1252454496dda62a03e5",
			x"6cbc4634d7e3c8f1a1532af7775cf9cbea96ca26115ccbb0c57e4773990e73bd",
			x"97410ff77ed6c668ff2fff29f9d6515f2efebc9d1791a362fd83b2dee74fe6ad",
			x"bfd3498511f18808ff98ff6fd931ff6d21572af69a15f7d515d8767f80aefa45",
			x"cf5360d6099a274c6c5f73282c6ac859b896a22d0b80d69c095f202acb072a3f",
			x"c77f2c1a2d5ec6708c7920da025b8128524daaa4c118ef3773f6d00ecc15be2f",
			x"bc9120807ad7cdecc19fc894a4b8ceee8c27091f7027bb4657bee6ec34e891bd",
			x"e6fb7847f3f04325e290e7142b3adb05a8de42e5365295008becefc6dcdd9ea6",
			x"e9cbe8bad47132a409e6d70ab0753eaf7ce84db314d4e34c5f215395371d2ece",
			x"b45f11d855e3b051d74404df4ad519e48b3025e8cc360b32f9b32137e0f3940f",
			x"cd6a504b07875cacf0049c23f45a4ae95de240410baf1d6610ac03478897389a",
			x"74b7847f30689f6bccc6ddcb323b77f305f454afbf558d0c2145c7237405a93f",
			x"caf231912aaa986a5c4b54a84642eb9eb6f2ba1cf703115e5049be13e869d039",
			x"2a452d02b5304b03c95e366946d5141f365cb5ed941e3b444c078de745c99780",
			x"6bd09458d0c200351a2380f629d9b97cfd485055297e229c4b8142283e6cf5cd",
			x"0d3064d39e7602c7cab9c7145099bb2ff7746277778eed7e0f7a576a91cbe6fa",
			x"fb4f18fc2576e24e6ddfbda0957e034d2b2b513bcd2ed785b4dc2491ab5f14e6",
			x"a3ab807204283afc3f24819055857dfb848040ac78a0db872b487a1abde4eeba",
			x"13bec3ba9008fe5541af76de60b99939100db108ba1e81a7229977490371a913",
			x"f2076b6e6e842e1e2f1de5a42049b011624725340ab8c7d0fa1e5a536e0490e3",
			x"f68277ffbabe4021b87497ac175dfdd81b929ee328fb984aba15e98af02c8bdc",
			x"7276877a2a93007b93a02261409cad327405d2044faf6b65fe9bf2b6c06981f4",
			x"309936aa0956f647c2f40c4344e7e27dde1cdd00b59f83d9d389ef1cb9991b5b",
			x"bb3f22e993b26aa74db3099bce36d4bbbcab441db152f43640400231e7083e99",
			x"547747d5e984ef21c59ae4b4f14873e0a5857f404f0c48c5555e6a1141cd5328",
			x"765d6f7d144180ef312c6ecd26404cef7c5f98f78f65cb8978f65b2988d19e5e",
			x"615b7611414d1a7f945a36811406623aee14b57839e4cbfb1163a7ce06ad553e",
			x"b7a319f8ba984d68ef1e754ac8e924b709c88784c4661f251c2c562fab3897a7",
			x"c8aec62c135834df428aaca515b86a427d73b7e44fcdb8a0a57832570e4a1e3a",
			x"00a68cebe4447dc3f91ae18bd1e073a69626b3221abcd25af065432f3d540786",
			x"bc7016ed56a919a4ffef498fc65bca7a2bd5aac1095d0e2a38e0375354e684c5",
			x"0f89bd464dca5cae4225b6cda388fbb970131b1ecd212ea1da3ad5154ef83381",
			x"8d05e14d0be42129ea91a40239b39027944a04cec6f6168396569c20f50b2bd7",
			x"8664aa6766f4f4e76de7aa0e06fe4d5e97b933cf1ea537c3c4b8258c798d7231",
			x"f3e260630144597e8d03c82a1a72a87f0f82be6fedb776617867c5e32fe4bd12",
			x"e3397669d405922d40174ba850b28a7ea058b156db53e91b0a987d2a8cba8ce7",
			x"8e0b0f12695e9e85c09c157c64d4cd5ea28ce20dc8c6300b2dd58345df0fef4a",
			x"0afa82ff776edc5ed25d399d892cace853eb133629428317c05cf55bbb2c8f29",
			x"cfc587270410523e3e48d57d4e4aa888f5de40426b66ae3b62cefac67df3c68b"
		);
	begin
		reset_n <= '0';
		wait for 20 ns;
		idle;
		wait for 10 ns;
		reset_n <= '1';

		for i in expected'range loop
			tick;
			init <= '1';
			tick;
			init <= '0';
			-- 0 -> no iterations, 1..4 -> one iteration, ...
			for j in 0 to (i + 3) / 4 - 1 loop
				update <= '1';
				data <= randombytes(j);
				if(j = (i + 3) / 4 - 1) then
					case i mod 4 is
						when 0 => be <= "1111";
						when 1 => be <= "1000";
						when 2 => be <= "1100";
						when 3 => be <= "1110";
					end case;
				else
					be <= "1111";
				end if;
				tick;
			end loop;
			update <= '0';
			final <= '1';
			tick;
			idle;

			wait until rising_edge(ck) and out_valid = '1';
			assert out_data = expected(i)(255 downto 224) report "wrong checksum for length " & to_string(i) severity error;
			wait until rising_edge(ck) and out_valid = '1';
			assert out_data = expected(i)(223 downto 192) report "wrong checksum for length " & to_string(i) severity error;
			wait until rising_edge(ck) and out_valid = '1';
			assert out_data = expected(i)(191 downto 160) report "wrong checksum for length " & to_string(i) severity error;
			wait until rising_edge(ck) and out_valid = '1';
			assert out_data = expected(i)(159 downto 128) report "wrong checksum for length " & to_string(i) severity error;
			wait until rising_edge(ck) and out_valid = '1';
			assert out_data = expected(i)(127 downto 96) report "wrong checksum for length " & to_string(i) severity error;
			wait until rising_edge(ck) and out_valid = '1';
			assert out_data = expected(i)(95 downto 64) report "wrong checksum for length " & to_string(i) severity error;
			wait until rising_edge(ck) and out_valid = '1';
			assert out_data = expected(i)(63 downto 32) report "wrong checksum for length " & to_string(i) severity error;
			wait until rising_edge(ck) and out_valid = '1';
			assert out_data = expected(i)(31 downto 0) report "wrong checksum for length " & to_string(i) severity error;
		end loop;

		wait for 10 ns;
		finish;
	end process;

end architecture;

set_time_format -unit ns -decimal_places 3

create_generated_clock -name debug_clk -source [get_pins {debug|debug_clk_out|ALTDDIO_OUT_component|auto_generated|ddio_outa[0]|muxsel}] [get_ports {debug_clk}]

derive_pll_clocks
derive_clock_uncertainty

#set_output_delay -max 5.1 -clock [get_clocks debug_clk] [get_ports {debug[*]}]
#set_output_delay -min 4.9 -clock [get_clocks debug_clk] [get_ports {debug[*]}]

set_output_delay -clock { debug_clk } 6 [get_ports {debug_data[*]}]
library IEEE;

use ieee.std_logic_1164.ALL;

entity tb_iomem_read is
end entity;

architecture sim of tb_iomem_read is
	signal reset : std_logic;
	signal clk : std_logic;

	signal rx_ready : std_logic;
	signal rx_valid : std_logic;
	signal rx_data : std_logic_vector(63 downto 0);
	signal rx_sop : std_logic;
	signal rx_eop : std_logic;
	signal rx_err : std_logic;
	signal rx_mask : std_logic;

	signal rx_bardec : std_logic_vector(7 downto 0);

	signal tx_req : std_logic;
	signal tx_start : std_logic;

	signal tx_ready : std_logic;
	signal tx_valid : std_logic;
	signal tx_data : std_logic_vector(63 downto 0);
	signal tx_sop : std_logic;
	signal tx_eop : std_logic;
	signal tx_err : std_logic;

	signal cpl_pending : std_logic;

begin
	clk <= '0' when (reset = '0') else not clk after 8 ns;

	process
	begin
		wait for 5 ns;
		reset <= '0';

		-- signals become valid in the middle of reset
		wait for 10 ns;
		rx_valid <= '0';
		rx_err <= '0';
		tx_ready <= '1';
		tx_start <= '0';

		-- deassert reset
		wait for 10 ns;
		reset <= '1';

		-- send tlp
		wait for 20 ns;
		wait until rising_edge(clk);
		rx_valid <= '1';
		rx_data <= x"000000FF40000002";
		rx_sop <= '1';
		rx_eop <= '0';
		wait until rising_edge(clk);
		rx_data <= x"00000000F7C00000";
		rx_sop <= '0';
		rx_bardec <= "00000001";
		wait until rising_edge(clk);
		rx_data <= x"0000000119C8A000";
		rx_eop <= '1';
		rx_bardec <= (others => 'U');
		wait until rising_edge(clk);
		rx_sop <= '1';
		rx_eop <= '0';
		rx_data <= x"000000FF00000002";
		wait until rising_edge(clk);
		rx_sop <= '0';
		rx_eop <= '1';
		rx_bardec <= "00000001";
		rx_data <= x"00000000F7C00000";
		wait until rising_edge(clk);
		rx_valid <= '0';
		rx_data <= (others => 'U');
		rx_bardec <= (others => 'U');
		rx_sop <= 'U';
		rx_eop <= 'U';
		wait until rising_edge(clk) and tx_req = '1';
		tx_start <= '1';
		wait until rising_edge(clk);
		tx_start <= '0';
		wait until (cpl_pending = '0') for 100 ns;
		wait for 50 ns;
		reset <= '0';
		wait;
	end process;

	registers : entity work.registers
		port map(
			reset => reset,
			clk => clk,
			cfg_address => "0000000100000000",
			rx_ready => rx_ready,
			rx_mask => rx_mask,
			rx_valid => rx_valid,
			rx_data => rx_data,
			rx_sop => rx_sop,
			rx_eop => rx_eop,
			rx_err => rx_err,
			rx_bardec => rx_bardec,
			tx_req => tx_req,
			tx_start => tx_start,
			tx_ready => tx_ready,
			tx_valid => tx_valid,
			tx_data => tx_data,
			tx_sop => tx_sop,
			tx_eop => tx_eop,
			tx_err => tx_err,
			cpl_pending => cpl_pending);
end architecture;

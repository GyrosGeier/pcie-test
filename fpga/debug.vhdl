library IEEE;

use IEEE.std_logic_1164.ALL;

entity debug is
	port(
		clk : in std_logic;
		data : in std_logic_vector(63 downto 0);
		data_valid : in std_logic;

		free_clk : in std_logic;

		debug_clk : out std_logic;
		debug_data : out std_logic_vector(7 downto 0)
	);
end entity;

architecture syn of debug is
	signal slowclk : std_logic;
	signal slowclk_locked : std_logic;
	signal slowdata : std_logic_vector(7 downto 0);
	signal slowdata_nvalid : std_logic;

begin
	debug_clk_gen : entity work.debug_pll
		port map(
			inclk0 => free_clk,
			locked => slowclk_locked,
			c0 => slowclk);

	debug_fifo : entity work.debug_fifo
		port map(
			data => data,
			wrclk => clk,
			wrreq => data_valid,

			q => slowdata,
			rdclk => slowclk,
			rdreq => slowclk_locked,
			rdempty => slowdata_nvalid);

	debug_clk_out : entity work.debug_clk_out
		port map(
			datain_h(0) => NOT slowdata_nvalid,
			datain_l => "0",
			outclock => slowclk,
			dataout(0) => debug_clk);

	debug_out : entity work.debug_out
		port map(
			datain => slowdata,
			dataout => debug_data);

end architecture;

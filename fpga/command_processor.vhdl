library ieee;
use ieee.std_logic_1164.ALL;

entity command_processor is
	port(
		-- clk/reset interface
		clk : in std_logic;
		reset_n : in std_logic;

		-- command fifo interface
		ready : out std_logic;
		valid : in std_logic;
		data : in std_logic_vector(63 downto 0);

		-- PCIe upstream rx interface (Avalon-ST w/ error)
		pcie_rx_ready : out std_logic;
		pcie_rx_valid : in std_logic;
		pcie_rx_data : in std_logic_vector(63 downto 0);
		pcie_rx_sop : in std_logic;
		pcie_rx_eop : in std_logic;
		pcie_rx_err : in std_logic;
		pcie_rx_bardec : in std_logic_vector(7 downto 0);

		-- mask function
		pcie_rx_mask : out std_logic;

		-- arbiter control signals
		pcie_tx_req : out std_logic;
		pcie_tx_start : in std_logic;

		-- PCIe upstream tx interface (Avalon-ST w/ error)
		pcie_tx_ready : in std_logic;
		pcie_tx_valid : out std_logic;
		pcie_tx_data : out std_logic_vector(63 downto 0);
		pcie_tx_sop : out std_logic;
		pcie_tx_eop : out std_logic;
		pcie_tx_err : out std_logic;

		pcie_cpl_pending : out std_logic;

		-- our address (bus/device/function)
		cfg_address : in std_logic_vector(15 downto 0);

		int : out std_logic
	);
end entity;

architecture rtl of command_processor is
	component sha_fifo
		PORT
		(
			aclr		: IN STD_LOGIC  := '0';
			data		: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
			rdclk		: IN STD_LOGIC ;
			rdreq		: IN STD_LOGIC ;
			wrclk		: IN STD_LOGIC ;
			wrreq		: IN STD_LOGIC ;
			q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
			rdempty		: OUT STD_LOGIC ;
			wrempty		: OUT STD_LOGIC 
		);
	end component;

	type command is (sha256_init, sha256_update, sha256_finalize);

	subtype qword is std_logic_vector(63 downto 0);

	-- opcodes
	constant cmd_sync		: qword := x"00000000_00000001";
	constant cmd_sha256_init	: qword := x"01000000_00000001";
	constant cmd_sha256_update	: qword := x"01000000_00000002";
	constant cmd_sha256_finalize	: qword := x"01000000_00000003";

	type state_t is (
		idle,
		-- sha256_init has no parameters (yet)
		sha256_update_expect_address, sha256_update_expect_size, sha256_update_wait_for_dma_start, sha256_update_wait_for_dma_end, sha256_update_wait_for_sha,
		sha256_finalize_expect_address, sha256_finalize_wait_for_sha, sha256_finalize_wait_for_dma,
		stuck);

	signal state : state_t;

	signal sha_ready : std_logic;

	signal sha_init : std_logic;
	signal sha_update : std_logic;
	signal sha_final : std_logic;

	signal sha_empty : std_logic;		-- FIFO is empty (i.e. data is not valid)

	signal sha_data : std_logic_vector(31 downto 0);
	signal sha_be : std_logic_vector(3 downto 0);

	signal sha_output_valid : std_logic;
	signal sha_output : std_logic_vector(31 downto 0);

	signal sha_result : std_logic_vector(255 downto 0);

	signal dma_read_address : qword;
	signal dma_read_size : qword;
	signal dma_read_strobe : std_logic;

	signal dma_read_rx_ready : std_logic;
	signal dma_read_rx_valid : std_logic;
	signal dma_read_rx_data : qword;
	signal dma_read_rx_sop : std_logic;
	signal dma_read_rx_eop : std_logic;
	signal dma_read_rx_err : std_logic;

	signal dma_read_rx_mask : std_logic;
	signal dma_read_rx_bardec : std_logic_vector(7 downto 0);

	signal dma_read_tx_req : std_logic;
	signal dma_read_tx_start : std_logic;
	signal dma_read_tx_ready : std_logic;
	signal dma_read_tx_valid : std_logic;
	signal dma_read_tx_data : qword;
	signal dma_read_tx_sop : std_logic;
	signal dma_read_tx_eop : std_logic;
	signal dma_read_tx_err : std_logic;
	signal dma_read_cpl_pending : std_logic;

	signal dma_read_data_valid : std_logic;
	signal dma_read_data : qword;
	signal dma_read_done : std_logic;

	signal dma_write_rx_ready : std_logic;
	signal dma_write_rx_valid : std_logic;
	signal dma_write_rx_data : qword;
	signal dma_write_rx_sop : std_logic;
	signal dma_write_rx_eop : std_logic;
	signal dma_write_rx_err : std_logic;

	signal dma_write_rx_mask : std_logic;
	signal dma_write_rx_bardec : std_logic_vector(7 downto 0);

	signal dma_write_tx_req : std_logic;
	signal dma_write_tx_start : std_logic;
	signal dma_write_tx_ready : std_logic;
	signal dma_write_tx_valid : std_logic;
	signal dma_write_tx_data : qword;
	signal dma_write_tx_sop : std_logic;
	signal dma_write_tx_eop : std_logic;
	signal dma_write_tx_err : std_logic;
	signal dma_write_cpl_pending : std_logic;

	signal dma_write_address : qword;
	constant dma_write_size : qword := x"00000000_00000020";	-- always 32 bytes
	signal dma_write_strobe : std_logic;
	signal dma_write_ready : std_logic;
	signal dma_write_valid : std_logic;
	signal dma_write_data : std_logic_vector(63 downto 0);
	signal dma_write_done : std_logic;

	type result_dword is (first, second, third, fourth, fifth, sixth, seventh, eighth);
	signal sha_result_state : result_dword;

	type result_qword is (first, second, third, fourth);
	signal dma_write_state : result_qword;

	-- get big endian data from PCIe
	function swap_be64(i : std_logic_vector(63 downto 0)) return std_logic_vector is
	begin
		return
			i(7 downto 0) & i(15 downto 8) & i(23 downto 16) & i(31 downto 24) &
			i(39 downto 32) & i(47 downto 40) & i(55 downto 48) & i(63 downto 56);
	end function;

	-- swap dwords for narrowing fifo
	function swap_dwords(i : std_logic_vector(63 downto 0)) return std_logic_vector is
	begin
		return i(31 downto 0) & i(63 downto 32);
	end function;
begin
	with state select ready <=
		'1' when idle,
		'1' when sha256_update_expect_address | sha256_update_expect_size,
		'0' when sha256_update_wait_for_dma_start | sha256_update_wait_for_dma_end | sha256_update_wait_for_sha,
		'1' when sha256_finalize_expect_address,
		'0' when sha256_finalize_wait_for_sha | sha256_finalize_wait_for_dma,
		'0' when stuck;

	process(clk, reset_n) is
	begin
		if(reset_n = '0') then
			state <= idle;
			sha_init <= '0';
			sha_update <= '0';
			sha_final <= '0';
			dma_read_strobe <= '0';
			dma_write_strobe <= '0';
			dma_write_valid <= '0';
			int <= '0';
		elsif(rising_edge(clk)) then
			sha_init <= '0';
			sha_final <= '0';
			dma_read_strobe <= '0';
			dma_write_strobe <= '0';
			int <= '0';
			case state is
				when idle =>
					if(valid = '1') then
						case data is
							when cmd_sync =>
								int <= '1';
							when cmd_sha256_init =>
								sha_init <= '1';
							when cmd_sha256_update =>
								state <= sha256_update_expect_address;
							when cmd_sha256_finalize =>
								state <= sha256_finalize_expect_address;
							when others =>
								state <= stuck;
								int <= '1';
						end case;
					end if;
				when sha256_update_expect_address =>
					if(valid = '1') then
						dma_read_address <= data;
						state <= sha256_update_expect_size;
					end if;
				when sha256_update_expect_size =>
					if(valid = '1') then
						dma_read_size <= data;
						dma_read_strobe <= '1';
						state <= sha256_update_wait_for_dma_start;
					end if;
				when sha256_update_wait_for_dma_start =>
					if(valid = '1') then
						-- should not happen
						report "command accepted while busy" severity error;
						state <= stuck;
						int <= '1';
					end if;
					if(sha_empty = '0') then
						state <= sha256_update_wait_for_dma_end;
					end if;
				when sha256_update_wait_for_dma_end =>
					if(valid = '1') then
						-- should not happen
						report "command accepted while busy" severity error;
						state <= stuck;
						int <= '1';
					end if;
					if(dma_read_done = '1') then
						state <= sha256_update_wait_for_sha;
					end if;
				when sha256_update_wait_for_sha =>
					if(valid = '1') then
						-- should not happen
						report "command accepted while busy" severity error;
						state <= stuck;
						int <= '1';
					end if;
					if(sha_empty = '1') then
						state <= idle;
					end if;
				when sha256_finalize_expect_address =>
					dma_write_address <= data;
					sha_final <= '1';
					state <= sha256_finalize_wait_for_sha;
					sha_result_state <= first;
				when sha256_finalize_wait_for_sha =>
					if(valid = '1') then
						-- should not happen
						report "command accepted while waiting for SHA256 block" severity error;
						state <= stuck;
						int <= '1';
					end if;
					case sha_result_state is
						when first =>
							if(sha_output_valid = '1') then
								sha_result(255 downto 224) <= sha_output;
								sha_result_state <= second;
							end if;
						when second =>
							if(sha_output_valid = '1') then
								sha_result(223 downto 192) <= sha_output;
								sha_result_state <= third;
							end if;
						when third =>
							if(sha_output_valid = '1') then
								sha_result(191 downto 160) <= sha_output;
								sha_result_state <= fourth;
							end if;
						when fourth =>
							if(sha_output_valid = '1') then
								sha_result(159 downto 128) <= sha_output;
								sha_result_state <= fifth;
							end if;
						when fifth =>
							if(sha_output_valid = '1') then
								sha_result(127 downto 96) <= sha_output;
								sha_result_state <= sixth;
							end if;
						when sixth =>
							if(sha_output_valid = '1') then
								sha_result(95 downto 64) <= sha_output;
								sha_result_state <= seventh;
							end if;
						when seventh =>
							if(sha_output_valid = '1') then
								sha_result(63 downto 32) <= sha_output;
								sha_result_state <= eighth;
							end if;
						when eighth =>
							if(sha_output_valid = '1') then
								sha_result(31 downto 0) <= sha_output;
								dma_write_strobe <= '1';
								dma_write_state <= first;
								dma_write_data <= swap_be64(sha_result(255 downto 192));
								dma_write_valid <= '1';
								state <= sha256_finalize_wait_for_dma;
							end if;
					end case;
				when sha256_finalize_wait_for_dma =>
					if(valid = '1') then
						-- should not happen
						report "command accepted while waiting for DMA" severity error;
						state <= stuck;
						int <= '1';
					end if;
					case dma_write_state is
						when first =>
							if(dma_write_ready = '1') then
								dma_write_state <= second;
								dma_write_data <= swap_be64(sha_result(191 downto 128));
							end if;
						when second =>
							if(dma_write_ready = '1') then
								dma_write_state <= third;
								dma_write_data <= swap_be64(sha_result(127 downto 64));
							end if;
						when third =>
							if(dma_write_ready = '1') then
								dma_write_state <= fourth;
								dma_write_data <= swap_be64(sha_result(63 downto 0));
							end if;
						when fourth =>
							if(dma_write_done = '1') then
								state <= idle;
								dma_write_valid <= '0';
							end if;
					end case;
				when stuck =>
					if(valid = '1') then
						-- should not happen
						report "command accepted while stuck" severity error;
						state <= unaffected;
					end if;
			end case;
		end if;
	end process;

	pcie_rx_ready <= dma_read_rx_ready and dma_write_rx_ready;
	pcie_rx_mask <= dma_read_rx_mask or dma_write_rx_mask;

	dma_read_rx_valid <= pcie_rx_valid;
	dma_read_rx_data <= pcie_rx_data;
	dma_read_rx_sop <= pcie_rx_sop;
	dma_read_rx_eop <= pcie_rx_eop;
	dma_read_rx_err <= pcie_rx_err;
	dma_read_rx_bardec <= pcie_rx_bardec;

	arbiter : entity work.pcie_arbiter
		generic map(
			num_agents => 2
		)
		port map(
			clk => clk,
			reset_n => reset_n,

			merged_tx_req => pcie_tx_req,
			merged_tx_start => pcie_tx_start,

			merged_tx_ready => pcie_tx_ready,
			merged_tx_valid => pcie_tx_valid,
			merged_tx_data => pcie_tx_data,
			merged_tx_sop => pcie_tx_sop,
			merged_tx_eop => pcie_tx_eop,
			merged_tx_err => pcie_tx_err,
		
			merged_cpl_pending => pcie_cpl_pending,

			arb_tx_req(1) => dma_read_tx_req,
			arb_tx_req(2) => dma_write_tx_req,
			arb_tx_start(1) => dma_read_tx_start,
			arb_tx_start(2) => dma_write_tx_start,
			arb_tx_ready(1) => dma_read_tx_ready,
			arb_tx_ready(2) => dma_write_tx_ready,
			arb_tx_valid(1) => dma_read_tx_valid,
			arb_tx_valid(2) => dma_write_tx_valid,
			arb_tx_data(1) => dma_read_tx_data,
			arb_tx_data(2) => dma_write_tx_data,
			arb_tx_sop(1) => dma_read_tx_sop,
			arb_tx_sop(2) => dma_write_tx_sop,
			arb_tx_eop(1) => dma_read_tx_eop,
			arb_tx_eop(2) => dma_write_tx_eop,
			arb_tx_err(1) => dma_read_tx_err,
			arb_tx_err(2) => dma_write_tx_err,
			arb_cpl_pending(1) => dma_read_cpl_pending,
			arb_cpl_pending(2) => dma_write_cpl_pending
		);

	dma_read : entity work.dma_read
		generic map(
			tag => x"01"
		)
		port map(
			clk => clk,
			reset_n => reset_n,
		
			cfg_address => cfg_address,

			rx_ready => dma_read_rx_ready,
			rx_valid => dma_read_rx_valid,
			rx_data => dma_read_rx_data,
			rx_sop => dma_read_rx_sop,
			rx_eop => dma_read_rx_eop,
			rx_err => dma_read_rx_err,
			rx_mask => dma_read_rx_mask,
			rx_bardec => dma_read_rx_bardec,

			tx_req => dma_read_tx_req,
			tx_start => dma_read_tx_start,

			tx_ready => dma_read_tx_ready,
			tx_valid => dma_read_tx_valid,
			tx_data => dma_read_tx_data,
			tx_sop => dma_read_tx_sop,
			tx_eop => dma_read_tx_eop,
			tx_err => dma_read_tx_err,

			cpl_pending => dma_read_cpl_pending,
		
			address => dma_read_address,
			size => dma_read_size,
			strobe => dma_read_strobe,

			data_valid => dma_read_data_valid,
			data_data => dma_read_data,

			dma_done => dma_read_done
		);

	sha_fifo_impl : sha_fifo
		port map(
			aclr => not reset_n,
			wrclk => clk,
			data => swap_dwords(swap_be64(dma_read_data)),
			wrreq => dma_read_data_valid,
			rdclk => clk,
			rdreq => sha_ready,
			q => sha_data,
			rdempty => sha_empty,
			wrempty => open
		);

	sha_be <= "1111";

	sha : entity work.sha256
		port map(
			ck => clk,
			reset_n => reset_n,

			ready => sha_ready,
			init => sha_init,
			update => not sha_empty,
			final => sha_final,
			data => sha_data,
			be => sha_be,

			out_data => sha_output,
			out_valid => sha_output_valid
		);

	dma_write : entity work.dma_write
		port map(
			clk => clk,
			reset => reset_n,	-- TODO fix name

			cfg_address => cfg_address,

			rx_ready => dma_write_rx_ready,
			rx_valid => dma_write_rx_valid,
			rx_data => dma_write_rx_data,
			rx_sop => dma_write_rx_sop,
			rx_eop => dma_write_rx_eop,
			rx_err => dma_write_rx_err,
			rx_mask => dma_write_rx_mask,
			rx_bardec => dma_write_rx_bardec,

			tx_ready => dma_write_tx_ready,
			tx_valid => dma_write_tx_valid,
			tx_data => dma_write_tx_data,
			tx_sop => dma_write_tx_sop,
			tx_eop => dma_write_tx_eop,
			tx_err => dma_write_tx_err,

			tx_req => dma_write_tx_req,
			tx_start => dma_write_tx_start,

			cpl_pending => dma_write_cpl_pending,

			address => dma_write_address,
			size => dma_write_size,
			strobe => dma_write_strobe,

			data_ready => dma_write_ready,
			data_valid => dma_write_valid,
			data_data => dma_write_data,
			
			dma_complete => dma_write_done
		);
end architecture;

library ieee;

use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity dma_write is
	port(
		clk : in std_logic;
		reset : in std_logic;

		cfg_address : in std_logic_vector(15 downto 0);

		rx_ready : out std_logic;
		rx_valid : in std_logic;
		rx_data : in std_logic_vector(63 downto 0);
		rx_sop : in std_logic;
		rx_eop : in std_logic;
		rx_err : in std_logic;
		rx_mask : out std_logic;
		-- valid during second cycle in 64 bit interface
		rx_bardec : in std_logic_vector(7 downto 0);

		tx_ready : in std_logic;
		tx_valid : out std_logic;
		tx_data : out std_logic_vector(63 downto 0);
		tx_sop : out std_logic;
		tx_eop : out std_logic;
		tx_err : out std_logic;

		tx_req : out std_logic;
		tx_start : in std_logic;

		cpl_pending : out std_logic;

		address : in std_logic_vector(63 downto 0);
		size : in std_logic_vector(63 downto 0);
		strobe : in std_logic;

		data_ready : out std_logic;
		data_valid : in std_logic;
		data_data : in std_logic_vector(63 downto 0);
		
		dma_complete : out std_logic
	);
end entity;

architecture syn of dma_write is
	type state is (idle, waiting, header, data);
	
	signal tx_state : state;

	signal remaining : unsigned(63 downto 0);
	
	constant nullptr : std_logic_vector(63 downto 0) := (others => '0');
begin
	rx_ready <= '1';
	rx_mask <= '0';
	cpl_pending <= '0';
	tx_err <= '0';
	
	tx_req <= '1' when (tx_state = waiting)
			else '0';
	
	dma_complete <= '1' when (tx_state = idle) else '0';

	data_ready <= '1' when (tx_state = data) else '0';
	
	process(reset, clk)
	begin
		if(reset = '0') then
			tx_state <= idle;

			tx_valid <= '0';
			tx_sop <= 'U';
			tx_eop <= 'U';
			tx_data <= (others => 'U');
		elsif(rising_edge(clk)) then
			if(tx_ready = '1') then
				case tx_state is
					when idle =>
						if(strobe = '1') then
							tx_state <= waiting;
							remaining <= unsigned(size);
						end if;
						tx_valid <= '0';
						tx_sop <= 'U';
						tx_eop <= 'U';
						tx_data <= (others => 'U');
					when waiting =>
						if (tx_start = '1') then
							tx_valid <= '1';
							tx_sop <= '1';
							tx_eop <= '0';
							tx_data <=
									cfg_address &			-- requester id
									"00000000" &			-- tag
									"11111111" &			-- byte enable
									"01100000" &			-- fmt/type
									"0" &						-- reserved
									"000" &					-- tc
									"0000" &					-- reserved
									"0" &						-- no digest
									"0" &						-- not poisoned
									"00" &					-- attributes
									"00" &					-- reserved
									size(11 downto 2);			-- length
							tx_state <= header;
						else
							tx_valid <= '0';
							tx_sop <= 'U';
							tx_eop <= 'U';
							tx_data <= (others => 'U');							
						end if;
					when header =>
						tx_valid <= '1';
						tx_sop <= '0';
						tx_eop <= '0';
						tx_data <=
								address(31 downto 2) &
								"00" &
								address(63 downto 32);
						tx_state <= data;
					when data =>
						if(data_valid = '1') then
							tx_valid <= '1';
							tx_sop <= '0';
							if(remaining <= 8) then
								tx_eop <= '1';
								tx_state <= idle;
							else
								tx_eop <= '0';
								tx_state <= unaffected;
							end if;
							remaining <= remaining - 8;
							tx_data <= data_data;
						else
							tx_valid <= '0';
						end if;
				end case;
			else
				tx_valid <= '0';
				tx_sop <= 'U';
				tx_eop <= 'U';
				tx_data <= (others => 'U');
			end if;
		end if;
	end process;
end architecture;

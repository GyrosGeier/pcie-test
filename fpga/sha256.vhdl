library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity sha256 is
	port(
		reset_n : in std_logic;
		ck : in std_logic;

		ready : out std_logic;
		init : in std_logic;
		update : in std_logic;
		final : in std_logic;
		data : in std_logic_vector(31 downto 0);
		be : in std_logic_vector(3 downto 0);

		out_data : out std_logic_vector(31 downto 0);
		out_valid : out std_logic
	);
end entity;

architecture rtl of sha256 is
	signal ready_int : std_logic;

	subtype uint32 is unsigned(31 downto 0);

	type initial_constants_t is array(0 to 7) of uint32;
	constant initial_constants : initial_constants_t :=
	(
		x"6a09e667", x"bb67ae85", x"3c6ef372", x"a54ff53a",
		x"510e527f", x"9b05688c", x"1f83d9ab", x"5be0cd19"
	);

	type round_constants_t is array(0 to 63) of uint32;
	constant k : round_constants_t :=
	(
		x"428a2f98", x"71374491", x"b5c0fbcf", x"e9b5dba5",
		x"3956c25b", x"59f111f1", x"923f82a4", x"ab1c5ed5",
		x"d807aa98", x"12835b01", x"243185be", x"550c7dc3",
		x"72be5d74", x"80deb1fe", x"9bdc06a7", x"c19bf174",
		x"e49b69c1", x"efbe4786", x"0fc19dc6", x"240ca1cc",
		x"2de92c6f", x"4a7484aa", x"5cb0a9dc", x"76f988da",
		x"983e5152", x"a831c66d", x"b00327c8", x"bf597fc7",
		x"c6e00bf3", x"d5a79147", x"06ca6351", x"14292967",
		x"27b70a85", x"2e1b2138", x"4d2c6dfc", x"53380d13",
		x"650a7354", x"766a0abb", x"81c2c92e", x"92722c85",
		x"a2bfe8a1", x"a81a664b", x"c24b8b70", x"c76c51a3",
		x"d192e819", x"d6990624", x"f40e3585", x"106aa070",
		x"19a4c116", x"1e376c08", x"2748774c", x"34b0bcb5",
		x"391c0cb3", x"4ed8aa4a", x"5b9cca4f", x"682e6ff3",
		x"748f82ee", x"78a5636f", x"84c87814", x"8cc70208",
		x"90befffa", x"a4506ceb", x"bef9a3f7", x"c67178f2"
	);

	-- padding output has no flow control
	signal padded_data : uint32;
	signal padded_valid : std_logic;
	signal padded_eop : std_logic;

	signal output_counter : natural range 7 downto 0;
	signal output_active : std_logic;
begin
	ready <= ready_int;

	-- message padding: after the data, append a single 1 bit,
	-- zeroes so that 64 bits remain until the next 512 bit boundary
	-- followed by the length in bits
	padding : process(reset_n, ck) is
		type padding_state is (copy, pad_zero, length_hi, length_lo, waiting, done);

		-- two state machines stacked on each other. This should be
		-- two processes, really, but the second introduces scheduling
		-- constraints that the first needs to be aware of anyway
		variable state : padding_state;
		variable phase : natural range 0 to 63;

		variable forwarding : boolean;

		type scrambler_history is array(-16 to 0) of uint32;
		variable w : scrambler_history;

		variable l : unsigned(63 downto 0);

		-- how many bytes are left in incomplete words (up to three)
		type leftover_state_t is (zero, one, two, three);
		variable leftover_state : leftover_state_t;
		variable leftover_data : std_logic_vector(23 downto 0);

		procedure ingest_byte(d : std_logic_vector(7 downto 0); v : std_logic) is
		begin
			if(v = '1') then
				case leftover_state is
					when zero =>
						leftover_data(23 downto 16) := d;
						leftover_state := one;
					when one =>
						leftover_data(15 downto 8) := d;
						leftover_state := two;
					when two =>
						leftover_data(7 downto 0) := d;
						leftover_state := three;
					when three =>
						w(0) := unsigned(leftover_data & d);
						forwarding := true;
						leftover_state := zero;
				end case;
				l := l + 8;
			end if;
		end procedure;

		variable s0, s1 : uint32;
	begin
		if(reset_n = '0') then
			forwarding := false;
			ready_int <= '1';
			leftover_state := zero;
		elsif rising_edge(ck) then
			if(init = '1') then
				state := copy;
				l := to_unsigned(0, l'length);
				phase := 0;
				w := (others => (others => 'U'));
				leftover_state := zero;
			end if;
			if(forwarding) then
				w(-16 to -1) := w(-15 to 0);
				w(0) := (others => 'U');
			end if;
			if(phase < 16) then
				case state is
					when copy =>
						-- copy the first 16 words to the output
						if(ready_int = '1' and update = '1') then
							forwarding := false;
							for i in 3 downto 0 loop
								ingest_byte(data(i * 8 + 7 downto i * 8), be(i));
							end loop;
						elsif(ready_int = '1' and final = '1') then
							forwarding := true;
							case leftover_state is
								when zero =>
									w(0) := x"80000000";
								when one =>
									w(0) := unsigned(leftover_data(23 downto 16) & x"800000");
								when two =>
									w(0) := unsigned(leftover_data(23 downto 8) & x"8000");
								when three =>
									w(0) := unsigned(leftover_data & x"80");
							end case;
							ready_int <= '0';
							leftover_state := zero;
							if(phase = 13) then
								state := length_hi;
							else
								state := pad_zero;
							end if;
						else
							forwarding := false;
						end if;
						if(forwarding and phase = 15) then
							ready_int <= '0';
						end if;
					when pad_zero =>
						w(0) := x"00000000";
						forwarding := true;
						if(phase = 13) then
							state := length_hi;
						end if;
					when length_hi =>
						w(0) := l(63 downto 32);
						forwarding := true;
						state := length_lo;
					when length_lo =>
						w(0) := l(31 downto 0);
						forwarding := true;
						state := waiting;
					when waiting =>
						forwarding := false;
						if(output_active = '1') then
							state := done;
						end if;
					when done =>
						forwarding := false;
						if(output_active = '0') then
							state := copy;
							ready_int <= '1';
						end if;
				end case;
			else
				s0 := (w(-15) ror  7) xor (w(-15) ror 18) xor (w(-15) srl  3);
				s1 := (w(- 2) ror 17) xor (w(- 2) ror 19) xor (w(- 2) srl 10);
				w(0) := w(-16) + s0 + w(-7) + s1;
				forwarding := true;
			end if;
			if(forwarding) then
				padded_data <= w(0);
				padded_valid <= '1';
				if(phase < 63) then
					phase := phase + 1;
					padded_eop <= '0';
				else
					phase := 0;
					if(state = waiting) then
						padded_eop <= '1';
					else
						padded_eop <= '0';
					end if;
					ready_int <= '1';
				end if;
			else
				padded_data <= (others => 'U');
				padded_valid <= '0';
				padded_eop <= '0';
			end if;
		end if;
	end process;

	process(reset_n, ck) is
	begin
		if(reset_n = '0') then
			output_counter <= 0;
			output_active <= '0';
		elsif(rising_edge(ck)) then
			if(padded_eop = '1' or output_counter > 0) then
				output_active <= '1';
			else
				output_active <= '0';
			end if;

			if(padded_eop = '1') then
				output_counter <= 7;
			elsif(output_counter > 0) then
				output_counter <= output_counter - 1;
			else
				output_counter <= 0;
			end if;
		end if;
	end process;

	hash : process(reset_n, ck) is
		variable phase : natural range 0 to 63;

		variable h0, h1, h2, h3, h4, h5, h6, h7 : uint32;
		variable a, b, c, d, e, f, g, h : uint32;
		variable S1, ch, temp1, S0, maj, temp2 : uint32;

		-- trade area for (theoretical) f_max by
		-- realizing the k array as shift registers.
		--
		-- cost: 2048 register-only LE
		-- benefit: r-r chain lengths reduced
		--
		-- currently, that is not the critical path, so the f_max
		-- benefit is purely theoretical
		--
		-- TODO: add a generic parameter for tuning
		-- TODO: this could be done with 256 LE
		variable k_pipeline : round_constants_t;
	begin
		if(reset_n = '0') then
			out_valid <= '0';
		elsif(rising_edge(ck) and init = '1') then
			h0 := initial_constants(0);
			h1 := initial_constants(1);
			h2 := initial_constants(2);
			h3 := initial_constants(3);
			h4 := initial_constants(4);
			h5 := initial_constants(5);
			h6 := initial_constants(6);
			h7 := initial_constants(7);
			phase := 0;
			k_pipeline := k;
			a := h0;
			b := h1;
			c := h2;
			d := h3;
			e := h4;
			f := h5;
			g := h6;
			h := h7;
			out_valid <= '0';
			out_data <= (others => 'U');
		elsif(rising_edge(ck) and padded_valid = '1') then
			S1 := (e ror 6) xor (e ror 11) xor (e ror 25);
			ch := (e and f) xor ((not e) and g);
			temp1 := h + S1 + ch + k_pipeline(0) + padded_data;
			S0 := (a ror 2) xor (a ror 13) xor (a ror 22);
			maj := (a and b) xor (a and c) xor (b and c);
			temp2 := S0 + maj;

			h := g;
			g := f;
			f := e;
			e := d + temp1;
			d := c;
			c := b;
			b := a;
			a := temp1 + temp2;

			if(phase = 63) then
				h0 := h0 + a;
				h1 := h1 + b;
				h2 := h2 + c;
				h3 := h3 + d;
				h4 := h4 + e;
				h5 := h5 + f;
				h6 := h6 + g;
				h7 := h7 + h;
				a := h0;
				b := h1;
				c := h2;
				d := h3;
				e := h4;
				f := h5;
				g := h6;
				h := h7;
				phase := 0;
				k_pipeline := k;
			else
				phase := phase + 1;
				k_pipeline := k_pipeline(1 to 63) & x"00000000";
			end if;

		elsif(rising_edge(ck) and output_active = '1') then
			assert phase = 0 report "end of stream reached in wrong phase" severity error;
			out_valid <= '1';
			out_data <= std_logic_vector(h0);
			h0 := h1;
			h1 := h2;
			h2 := h3;
			h3 := h4;
			h4 := h5;
			h5 := h6;
			h6 := h7;
			h7 := h0;
		elsif(rising_edge(ck)) then
			out_valid <= '0';
			out_data <= (others => 'U');
		end if;
	end process;
end architecture;

library ieee;
use ieee.std_logic_1164.ALL;

entity sha_fifo is
	port (
		aclr		: in std_logic;
		data		: in std_logic_vector(63 DOWNTO 0);
		rdclk		: in std_logic;
		rdreq		: in std_logic;
		wrclk		: in std_logic;
		wrreq		: in std_logic;
		q		: out std_logic_vector(31 DOWNTO 0);
		rdempty		: out std_logic;
		wrempty		: out std_logic 
	);
end entity;

architecture sim of sha_fifo is
	subtype address is integer range 0 to 15;

	signal writep, readp : address;

	type contents_t is array(address) of std_logic_vector(31 downto 0);
	signal contents : contents_t;
begin
	wrempty <= rdempty;

	process(aclr, rdclk, wrclk) is
	begin
		if(aclr = '1') then
			writep <= 0;
			readp <= 0;
			rdempty <= '1';
		else
			if(rising_edge(rdclk)) then
				q <= contents(readp);
				if(readp < writep and rdreq = '1') then
					readp <= readp + 1;
				end if;
				rdempty <= '1' when readp = writep else '0';
			end if;
			if(rising_edge(wrclk)) then
				if(wrreq = '1') then
					contents(writep) <= data(63 downto 32);
					contents(writep + 1) <= data(31 downto 0);
					writep <= writep + 2;
				end if;
			end if;
		end if;
	end process;
end architecture;

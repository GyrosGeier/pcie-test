#! /bin/sh -e

enable_tbbmalloc_workaround()
{
	LD_PRELOAD='/opt/${LIB}/hack.so'
	export LD_PRELOAD
}

disable_tbbmalloc_workaround()
{
	unset LD_PRELOAD
}

PATH=/opt/altera/16.1/quartus/bin:$PATH
if [ "$1" = "-r" ]; then
	if [ -z "$DISPLAY" ]; then
		if [ -z "`which xvfb-run`" ]; then
			echo >&2 "MegaWizard requires an X server."
			echo >&2
			echo >&2 "Either run from a graphical session or install xvfb"
			exit 1
		fi
		# reexec self under xvfb
		exec xvfb-run $0 "$*"
	fi
	mw-regenerate cmd_fifo.vhd
	mw-regenerate debug_clk_out.vhd
	mw-regenerate debug_fifo.vhd
	mw-regenerate debug_out.vhd
	mw-regenerate debug_pll.vhd
	mw-regenerate pcie_reconfig.vhd
	mw-regenerate pcie_serdes.vhd
	mw-regenerate pcie.vhd
	mw-regenerate pll.vhd
	mw-regenerate sha_fifo.vhd
	# patch frequency of fixedclk_serdes
	sed -i -e '/fixedclk_serdes/s/100/125/' pcie.sdc
	# patch multicycle constraints for tl_cfg sampling
	sed -i -e '/set_multicycle_path/s/tl_cfg_[a-z_]*/&_hip/' pcie.sdc
	# copy tl_cfg sampling code
	cp pcie_examples/chaining_dma/altpcierd_tl_cfg_sample.vhd .
	cp pcie_examples/chaining_dma/pcie_rs_hip.vhd .
fi

enable_tbbmalloc_workaround

quartus_map pcie_test
quartus_fit pcie_test
quartus_asm pcie_test
quartus_cpf --convert --frequency=10MHz --voltage=3.3V --operation=p output_files/pcie_test.sof output_files/pcie_test.svf
quartus_sta pcie_test
quartus_eda pcie_test

disable_tbbmalloc_workaround

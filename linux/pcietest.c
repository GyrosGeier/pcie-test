#include <linux/module.h>
#include <linux/pci.h>

#include <asm/delay.h>	/* todo remove */

static struct pci_device_id const pcie_test_ids[] =
{
	{ PCI_DEVICE(0x1172, 0x1337) },
	{ 0, }
};

struct pcie_test_private
{
	u64 volatile *reg;
	void *buf;
	dma_addr_t dma;
	int irq;
};

static irqreturn_t pcie_test_irq(int irq, void *cookie)
{
	struct pci_dev *dev;
	struct pcie_test_private *priv;
	u64 volatile *buf;
	unsigned int i;

	dev = (struct pci_dev *)cookie;
	priv = dev->dev.driver_data;

	printk(KERN_INFO "IRQ!\n");

	pci_dma_sync_single_for_cpu(dev, priv->dma, 32, DMA_FROM_DEVICE);

	buf = (u64 *)priv->buf;

	printk(KERN_INFO "  contents in irq:\n");
	for(i = 0; i < 48; i += 4)
		printk(KERN_INFO "    %llx %llx %llx %llx\n", buf[i+0], buf[i+1], buf[i+2], buf[i+3]);

	return IRQ_HANDLED;
}

static int pcie_test_probe(
		struct pci_dev *dev,
		struct pci_device_id const *id)
{
	int err;
	struct pcie_test_private *priv;
	u64 volatile *buf;

	unsigned int i, j;

	err = pcim_enable_device(dev);
	if(err < 0)
		return err;

	priv = devm_kzalloc(&dev->dev, sizeof *priv, GFP_KERNEL);
	if(!priv)
		return -ENOMEM;

	dev->dev.driver_data = priv;

	err = pci_set_dma_mask(dev, DMA_BIT_MASK(64));
	if(err < 0)
		return err;

	err = pci_set_consistent_dma_mask(dev, DMA_BIT_MASK(64));
	if(err < 0)
		return err;

	pci_set_master(dev);

	priv->reg = pcim_iomap(dev, 0, 256);

	printk(KERN_INFO "Hello %px\n", priv->reg);

	err = pci_alloc_irq_vectors(dev, 1, 1, PCI_IRQ_ALL_TYPES);
	if(err < 0)
		return err;

	priv->irq = pci_irq_vector(dev, 0);

	err = devm_request_irq(&dev->dev, priv->irq, &pcie_test_irq, IRQF_SHARED, "PCIe Test", dev);
	if(err < 0)
		return err;

	priv->buf = dmam_alloc_coherent(&dev->dev, 4096, &priv->dma, GFP_KERNEL);
	if(!priv->buf)
		return -ENOMEM;

	printk(KERN_INFO "  buffer %llx %px\n", priv->dma, priv->buf);

	buf = (u64 *)priv->buf;

	j = 0;

	buf[j++] = 0x0100000000000001ULL;	/* INIT command */
	buf[j++] = 0x0100000000000002ULL;	/* UPDATE command */
	buf[j++] = priv->dma + 128;		/* address */
	buf[j++] = 8;				/* size */
	buf[j++] = 0x0100000000000002ULL;	/* UPDATE command */
	buf[j++] = priv->dma + 136;		/* address */
	buf[j++] = 8;				/* size */
	buf[j++] = 0x0100000000000003ULL;	/* FINAL command */
	buf[j++] = priv->dma + 256;		/* output address */
	buf[j++] = 0x0000000000000001ULL;	/* SYNC command */
	
	buf[16] = 0xb8e83cfa894c84feULL;
	buf[17] = 0xefe0dc0339fd73c3ULL;

	printk(KERN_INFO "  contents initial:\n");
	for(i = 0; i < 48; i += 4)
		printk(KERN_INFO "    %llx %llx %llx %llx\n", buf[i+0], buf[i+1], buf[i+2], buf[i+3]);

	priv->reg[0] = cpu_to_le64(priv->dma);
	priv->reg[1] = cpu_to_le64(j);

	printk(KERN_INFO "  readback %llx %llx\n", priv->dma, le64_to_cpu(priv->reg[0]));

	printk(KERN_INFO "  contents 0us:\n");
	for(i = 0; i < 48; i += 4)
		printk(KERN_INFO "    %llx %llx %llx %llx\n", buf[i+0], buf[i+1], buf[i+2], buf[i+3]);

	udelay(25);

	printk(KERN_INFO "  contents 25us:\n");
	for(i = 0; i < 48; i += 4)
		printk(KERN_INFO "    %llx %llx %llx %llx\n", buf[i+0], buf[i+1], buf[i+2], buf[i+3]);

	return 0;
}

static void pcie_test_remove(
		struct pci_dev *dev)
{
	struct pcie_test_private *priv;

	priv = dev->dev.driver_data;

	pci_clear_master(dev);

	//dmam_free_coherent(&dev->dev, 4096, priv->buf, priv->dma);

	//free_irq(priv->irq, dev);
	//pci_free_irq_vectors(dev);

	//pci_disable_device(dev);

	printk(KERN_INFO "Goodbye\n");
}

static struct pci_driver pcie_test_driver =
{
	.name = "pcietest",
	.id_table = pcie_test_ids,
	.probe = pcie_test_probe,
	.remove = pcie_test_remove,
};

static int __init pcie_test_init(void)
{
	return pci_register_driver(&pcie_test_driver);
}

static void __exit pcie_test_exit(void)
{
	pci_unregister_driver(&pcie_test_driver);
}

module_init(pcie_test_init);
module_exit(pcie_test_exit);

MODULE_AUTHOR("Simon Richter <Simon.Richter@hogyros.de>");
MODULE_DESCRIPTION("PCIe device test / PCIe test device");
MODULE_LICENSE("GPL");
MODULE_DEVICE_TABLE(pci, pcie_test_ids);
